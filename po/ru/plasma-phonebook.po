# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-phonebook package.
#
# Alexander Yavorsky <kekcuha@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-phonebook\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-02 00:46+0000\n"
"PO-Revision-Date: 2021-10-04 14:34+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Александр Яворский"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kekcuha@gmail.com"

#: src/contents/ui/AddContactPage.qml:35
#, kde-format
msgid "Adding contact"
msgstr "Добавление контакта"

#: src/contents/ui/AddContactPage.qml:39
#, kde-format
msgid "Editing contact"
msgstr "Редактирование контакта"

#: src/contents/ui/AddContactPage.qml:58
#, kde-format
msgid "Photo"
msgstr "Фотография"

#: src/contents/ui/AddContactPage.qml:94
#, kde-format
msgid "Name:"
msgstr "Имя:"

#: src/contents/ui/AddContactPage.qml:112
#, kde-format
msgid "Phone:"
msgstr "Телефон:"

#: src/contents/ui/AddContactPage.qml:148
#, kde-format
msgid "+1 555 2368"
msgstr "+7 900 123-23-68"

#: src/contents/ui/AddContactPage.qml:184
#, kde-format
msgid "E-mail:"
msgstr "Адрес эл. почты:"

#: src/contents/ui/AddContactPage.qml:221
#, kde-format
msgid "user@example.org"
msgstr "user@example.org"

#: src/contents/ui/AddContactPage.qml:257
#, kde-format
msgid "Instant Messenger:"
msgstr "Мгновенные сообщения:"

#: src/contents/ui/AddContactPage.qml:293
#, kde-format
msgid "protocol:person@example.com"
msgstr "protocol:person@example.com"

#: src/contents/ui/AddContactPage.qml:328 src/contents/ui/DetailPage.qml:172
#, kde-format
msgid "Birthday:"
msgstr "День рождения:"

#: src/contents/ui/ContactsPage.qml:20 src/contents/ui/main.qml:17
#: src/main.cpp:47
#, kde-format
msgid "Phonebook"
msgstr "Телефонная книга"

#: src/contents/ui/ContactsPage.qml:24
#, kde-format
msgid "Create New"
msgstr "Создать"

#: src/contents/ui/ContactsPage.qml:33
#, kde-format
msgid "Import contacts"
msgstr "Импорт контактов"

#: src/contents/ui/ContactsPage.qml:77
#, kde-format
msgid "No contacts"
msgstr "Нет ни одного контакта"

#: src/contents/ui/DetailPage.qml:119
#, kde-format
msgid "Call"
msgstr "Вызов"

#: src/contents/ui/DetailPage.qml:128
#, kde-format
msgid "Select number to call"
msgstr "Выберите номер для вызова"

#: src/contents/ui/DetailPage.qml:135
#, kde-format
msgid "Send SMS"
msgstr "Отправить SMS"

#: src/contents/ui/DetailPage.qml:144
#, kde-format
msgid "Select number to send message to"
msgstr "Выберите номер для отправления SMS"

#: src/contents/ui/DetailPage.qml:151
#, kde-format
msgid "Send email"
msgstr "Отправить письмо"

#: src/contents/ui/DetailPage.qml:180
#, kde-format
msgid "Edit"
msgstr "Редактировать"

#: src/contents/ui/DetailPage.qml:188
#, kde-format
msgid "Delete contact"
msgstr "Удалить контакт"

#: src/contents/ui/DetailPage.qml:196
#, kde-format
msgid "Cancel"
msgstr "Отмена"

#: src/kpeopleactionplugin/kpeopleactionsplugin.cpp:40
#, kde-format
msgctxt "Action to write to instant messanger contact"
msgid "%1 %2"
msgstr "%1 %2"

#: src/main.cpp:47
#, kde-format
msgid "View and edit contacts"
msgstr "Просмотр и редактирование контактов"

#~ msgid "Save"
#~ msgstr "Сохранить"

#~ msgid "Address Book"
#~ msgstr "Адресная книга"

#~ msgid "Chat"
#~ msgstr "Чат"

#~ msgid "Video Call"
#~ msgstr "Видеовызов"

#~ msgid "Email"
#~ msgstr "Эл. письмо"

#~ msgid "Other"
#~ msgstr "Прочее"

#~ msgctxt "Action to tell user to call person using phone number"
#~ msgid "Call on %1"
#~ msgstr "Позвонить на %1"

#~ msgctxt "Action to tell user to write a message to phone number"
#~ msgid "Write SMS on %1"
#~ msgstr "СМС на %1"

#~ msgctxt "Action to send an email"
#~ msgid "email %1"
#~ msgstr "Эл. письмо %1"
