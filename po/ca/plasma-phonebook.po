# Translation of plasma-phonebook.po to Catalan
# Copyright (C) 2019-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2019, 2021, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-phonebook\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-02 00:46+0000\n"
"PO-Revision-Date: 2022-01-25 09:51+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: src/contents/ui/AddContactPage.qml:35
#, kde-format
msgid "Adding contact"
msgstr "Afegeix un contacte"

#: src/contents/ui/AddContactPage.qml:39
#, kde-format
msgid "Editing contact"
msgstr "Edita el contacte"

#: src/contents/ui/AddContactPage.qml:58
#, kde-format
msgid "Photo"
msgstr "Foto"

#: src/contents/ui/AddContactPage.qml:94
#, kde-format
msgid "Name:"
msgstr "Nom:"

#: src/contents/ui/AddContactPage.qml:112
#, kde-format
msgid "Phone:"
msgstr "Telèfon:"

#: src/contents/ui/AddContactPage.qml:148
#, kde-format
msgid "+1 555 2368"
msgstr "+1 555 2368"

#: src/contents/ui/AddContactPage.qml:184
#, kde-format
msgid "E-mail:"
msgstr "Correu electrònic:"

#: src/contents/ui/AddContactPage.qml:221
#, kde-format
msgid "user@example.org"
msgstr "usuari@exemple.cat"

#: src/contents/ui/AddContactPage.qml:257
#, kde-format
msgid "Instant Messenger:"
msgstr "Missatgeria instantània:"

#: src/contents/ui/AddContactPage.qml:293
#, kde-format
msgid "protocol:person@example.com"
msgstr "protocol:persona@exemple.cat"

#: src/contents/ui/AddContactPage.qml:328 src/contents/ui/DetailPage.qml:172
#, kde-format
msgid "Birthday:"
msgstr "Data de naixement:"

#: src/contents/ui/ContactsPage.qml:20 src/contents/ui/main.qml:17
#: src/main.cpp:47
#, kde-format
msgid "Phonebook"
msgstr "Agenda telefònica"

#: src/contents/ui/ContactsPage.qml:24
#, kde-format
msgid "Create New"
msgstr "Crea nou"

#: src/contents/ui/ContactsPage.qml:33
#, kde-format
msgid "Import contacts"
msgstr "Importa contactes"

#: src/contents/ui/ContactsPage.qml:77
#, kde-format
msgid "No contacts"
msgstr "No hi ha cap contacte"

#: src/contents/ui/DetailPage.qml:119
#, kde-format
msgid "Call"
msgstr "Trucada"

#: src/contents/ui/DetailPage.qml:128
#, kde-format
msgid "Select number to call"
msgstr "Seleccioneu el número a trucar"

#: src/contents/ui/DetailPage.qml:135
#, kde-format
msgid "Send SMS"
msgstr "Envia un SMS"

#: src/contents/ui/DetailPage.qml:144
#, kde-format
msgid "Select number to send message to"
msgstr "Selecciona el número a on enviar el missatge"

#: src/contents/ui/DetailPage.qml:151
#, kde-format
msgid "Send email"
msgstr "Envia un correu"

#: src/contents/ui/DetailPage.qml:180
#, kde-format
msgid "Edit"
msgstr "Edita"

#: src/contents/ui/DetailPage.qml:188
#, kde-format
msgid "Delete contact"
msgstr "Suprimeix el contacte"

#: src/contents/ui/DetailPage.qml:196
#, kde-format
msgid "Cancel"
msgstr "Cancel·la"

#: src/kpeopleactionplugin/kpeopleactionsplugin.cpp:40
#, kde-format
msgctxt "Action to write to instant messanger contact"
msgid "%1 %2"
msgstr "%1 %2"

#: src/main.cpp:47
#, kde-format
msgid "View and edit contacts"
msgstr "Visualitza i edita els contactes"

#~ msgid "Save"
#~ msgstr "Desa"

#~ msgid "Address Book"
#~ msgstr "Agenda d'adreces"

#~ msgid "Chat"
#~ msgstr "Xat"

#~ msgid "Video Call"
#~ msgstr "Trucada de vídeo"

#~ msgid "Email"
#~ msgstr "Correu electrònic"

#~ msgid "Other"
#~ msgstr "Altres"

#~ msgctxt "Action to tell user to call person using phone number"
#~ msgid "Call on %1"
#~ msgstr "Truca al %1"

#~ msgctxt "Action to tell user to write a message to phone number"
#~ msgid "Write SMS on %1"
#~ msgstr "Escriu un SMS al %1"

#~ msgctxt "Action to send an email"
#~ msgid "email %1"
#~ msgstr "correu electrònic %1"
